package main

import (
	"fmt"
	"gitee.com/qwert/common"
	"gitee.com/qwert/microservice_study/payment/domain/repository"
	service2 "gitee.com/qwert/microservice_study/payment/domain/service"
	"gitee.com/qwert/microservice_study/payment/handler"
	pb "gitee.com/qwert/microservice_study/payment/proto/payment"
	"github.com/go-micro/plugins/v4/registry/consul"
	"github.com/go-micro/plugins/v4/wrapper/monitoring/prometheus"
	ratelimit "github.com/go-micro/plugins/v4/wrapper/ratelimiter/uber"
	opentracing2 "github.com/go-micro/plugins/v4/wrapper/trace/opentracing"
	"github.com/opentracing/opentracing-go"
	"go-micro.dev/v4"
	"go-micro.dev/v4/logger"
	"go-micro.dev/v4/registry"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

var (
	service = "payment"
	version = "latest"
)

func main() {
	// 配置中心
	consulConfig, err := common.GetConsulConfig("192.168.2.104", 8500, "/micro/config")
	if err != nil {
		common.Error(err)
	}
	//注册中心
	consul := consul.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{
			"192.168.2.104:8500",
		}
	})
	//jaeger 链路追踪
	t, io, err := common.NewTracer("payment", "192.168.42.1:6831")
	if err != nil {
		common.Error(err)
	}
	defer io.Close()
	opentracing.SetGlobalTracer(t)

	//获取mysql配置,路径中不带前缀
	mysqlInfo, err := common.GetMysqlFromConsul(consulConfig, "mysql")
	if err != nil {
		logger.Fatal(err)
	}
	logger.Info("Mysql配置信息:", mysqlInfo)
	//创建数据库连接
	mysqlConfig := mysql.Config{
		DSN:                       fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", mysqlInfo.User, mysqlInfo.Pwd, mysqlInfo.Host, mysqlInfo.Port, mysqlInfo.Database),
		DefaultStringSize:         191,
		SkipInitializeWithVersion: false, // 根据版本自动配置
	}
	db, _ := gorm.Open(mysql.New(mysqlConfig), &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: true,
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})
	sqlDb, err := db.DB()
	if err != nil {
		return
	}
	defer sqlDb.Close()
	//创建表
	tableInit := repository.NewPaymentRepository(db)
	tableInit.InitTable()
	//监控
	common.PrometheusBoot(9089)
	// Create service
	srv := micro.NewService()
	// Initialise service
	srv.Init(
		micro.Name(service),
		micro.Version(version),
		micro.Address("0.0.0.0:8089"),
		//添加注册中心
		micro.Registry(consul),
		//添加链路追踪
		micro.WrapHandler(opentracing2.NewHandlerWrapper(opentracing.GlobalTracer())),
		//加载限流
		micro.WrapHandler(ratelimit.NewHandlerWrapper(1000)),
		//加载监控
		micro.WrapHandler(prometheus.NewHandlerWrapper()),
	)

	paymentDataService := service2.NewPaymentDataService(repository.NewPaymentRepository(db))

	// Register handler
	if err := pb.RegisterPaymentHandler(srv.Server(), &handler.Payment{PaymentDataService: paymentDataService}); err != nil {
		logger.Fatal(err)
	}
	// Run service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
