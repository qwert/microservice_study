package main

import (
	"category/common"
	"category/domain/repository"
	service2 "category/domain/service"
	"category/handler"
	"category/internal"
	"fmt"

	pb "category/proto"
	"go-micro.dev/v4/registry"
	"go-micro.dev/v4/util/log"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	"github.com/go-micro/plugins/v4/registry/consul"
	"go-micro.dev/v4"
	"go-micro.dev/v4/logger"
)

var (
	service = "category"
	version = "latest"
)

func main() {
	//注册中心
	consulRegistry := consul.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{
			"127.0.0.1:8500",
		}
	})
	//配置中心
	consulConfig, err := common.GetConsulConfig()
	if err != nil {
		log.Error(err)
		return
	}

	// Create service
	srv := micro.NewService()
	srv.Init(
		micro.Name(service),
		micro.Version(version),
		//这里设置地址和需要暴露的端口
		micro.Address("127.0.0.1:8082"),
		//添加consul 作为注册中心
		micro.Registry(consulRegistry),
	)
	//获取mysql配置,路径中不带前缀
	mysqlInfo, err := common.GetMysqlFromConsul(consulConfig, "mysql")
	if err != nil {
		logger.Fatal(err)
	}
	logger.Info("Mysql配置信息:", mysqlInfo)
	//创建数据库连接
	mysqlConfig := mysql.Config{
		DSN:                       fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", mysqlInfo.User, mysqlInfo.Pwd, mysqlInfo.Host, mysqlInfo.Port, mysqlInfo.Database),
		DefaultStringSize:         191,
		SkipInitializeWithVersion: false, // 根据版本自动配置
	}
	db, _ := gorm.Open(mysql.New(mysqlConfig), internal.Gorm.Config())
	sqlDb, err := db.DB()
	if err != nil {
		return
	}
	defer sqlDb.Close()
	//只执行一次，数据表初始化
	rp := repository.NewCategoryRepository(db)
	rp.InitTable()
	//initialise service
	categoryDataService := service2.NewCategoryDataService(rp)
	// Register handler
	if err = pb.RegisterCategoryHandler(srv.Server(), &handler.Category{CategoryDataService: categoryDataService}); err != nil {
		logger.Fatal(err)
	}

	// Run service
	if err = srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
