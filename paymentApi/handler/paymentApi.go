package handler

import (
	"context"
	"errors"
	"fmt"
	"gitee.com/qwert/common"
	"gitee.com/qwert/microservice_study/payment/proto/payment"
	paymentApi "gitee.com/qwert/microservice_study/paymentApi/proto/paymentApi"
	"github.com/plutov/paypal/v3"
	"strconv"
)

type PaymentApi struct {
	PaymentService payment.PaymentService
}

func (e *PaymentApi) Call(ctx context.Context, request *paymentApi.Request, response *paymentApi.Response) error {
	//TODO implement me
	panic("implement me")
}

var (
	ClientId = "AdxuNE1hVnjHI_WPK3FUvwyMz-7trujYC5TmpD-7bNg8F7jLVSk-MapHbGd3Y-egt07atulLT0E5-wCw"
)

func (e *PaymentApi) PayPalRefund(ctx context.Context, req *paymentApi.Request, rsp *paymentApi.Response) error {
	fmt.Println("-------")
	if err := isOk("payment_id", req); err != nil {
		rsp.StatusCode = 500
		return err
	}
	//退款号
	if err := isOk("refund_id", req); err != nil {
		rsp.StatusCode = 500
		return err
	}
	//验证退款金额
	if err := isOk("money", req); err != nil {
		rsp.StatusCode = 500
		return err
	}
	//获取paymentId
	payId, err := strconv.ParseInt(req.Get["payment_id"].Value[0], 10, 64)
	if err != nil {
		common.Error(err)
		return err
	}
	//获取支付通道信息
	paymentInfo, err := e.PaymentService.FindPaymentByID(ctx, &payment.PaymentID{PaymentId: payId})
	if err != nil {
		common.Error(err)
		return err
	}
	//获取Sid
	//支付模式
	status := paypal.APIBaseSandBox
	if paymentInfo.PaymentStatus {
		status = paypal.APIBaseLive
	}
	//退款例子
	payout := paypal.Payout{
		SenderBatchHeader: &paypal.SenderBatchHeader{
			EmailSubject: req.Get["refund_id"].Value[0] + "peng 提醒你有收款",
			EmailMessage: req.Get["refund_id"].Value[0] + "你有一个收款信息",
			//每笔转账都要唯一
			SenderBatchID: req.Get["refund_id"].Value[0] + "",
		},
		Items: []paypal.PayoutItem{
			{
				RecipientType: "EMA",
				//RecipientWallet: "",
				Receiver: "sb-ketg4322333009@business.example.com",
				Amount: &paypal.AmountPayout{
					Currency: "SUD",
					Value:    req.Get["money"].Value[0],
				},
				Note:         req.Get["refund_id"].Value[0],
				SenderItemID: req.Get["refund_id"].Value[0],
			},
		},
	}
	//创建支付客户端
	paymentClient, err := paypal.NewClient(ClientId, paymentInfo.PaymentSid, status)
	if err != nil {
		common.Error(err)
		return err
	}
	//获取token
	_, err = paymentClient.GetAccessToken()
	if err != nil {
		common.Error(err)
	}
	paymentResult, err := paymentClient.CreateSinglePayout(payout)
	if err != nil {
		common.Error(err)
	}
	common.Info(paymentResult)
	rsp.Body = req.Get["money"].Value[0] + "支付成功"
	return err
}

func isOk(key string, req *paymentApi.Request) error {
	if _, ok := req.Get[key]; !ok {

		err := errors.New("payment_id 参数错误")
		common.Error(err)
		return err
	}
	return nil
}
