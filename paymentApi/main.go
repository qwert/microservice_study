package main

import (
	"context"
	"fmt"
	"gitee.com/qwert/common"
	"gitee.com/qwert/microservice_study/payment/proto/payment"
	"gitee.com/qwert/microservice_study/paymentApi/handler"
	paymentApi "gitee.com/qwert/microservice_study/paymentApi/proto/paymentApi"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/go-micro/plugins/v4/registry/consul"
	"github.com/go-micro/plugins/v4/wrapper/select/roundrobin"
	opentracing2 "github.com/go-micro/plugins/v4/wrapper/trace/opentracing"
	"github.com/opentracing/opentracing-go"
	"go-micro.dev/v4"
	"go-micro.dev/v4/client"
	"go-micro.dev/v4/logger"
	"go-micro.dev/v4/registry"
	"go-micro.dev/v4/util/log"
	"net"
	"net/http"
)

var (
	service = "paymentapi"
	version = "latest"
)

func main() {

	//注册中心
	consul := consul.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{
			"192.168.2.104:8500",
		}
	})
	//jaeger 链路追踪
	t, io, err := common.NewTracer("payment", "192.168.2.104:6831")
	if err != nil {
		common.Error(err)
	}
	defer io.Close()
	opentracing.SetGlobalTracer(t)
	//熔断
	hystrixStreamHandler := hystrix.NewStreamHandler()
	hystrixStreamHandler.Start()
	//启动监听
	go func() {
		err = http.ListenAndServe(net.JoinHostPort("0.0.0.0", "9192"), hystrixStreamHandler)
		common.Error(err)
		log.Error(err)
	}()
	//监控
	common.PrometheusBoot(9292)
	// Create service
	srv := micro.NewService()
	srv.Init(
		micro.Name(service),
		micro.Version(version),
		micro.Address("0.0.0.0:9092"),
		//添加consul 注册中心
		micro.Registry(consul),
		//添加链路追踪
		micro.WrapHandler(opentracing2.NewHandlerWrapper(opentracing.GlobalTracer())),
		//添加熔断
		micro.WrapClient(NewClientHystrixWrapper()),
		//添加负载均衡
		micro.WrapClient(roundrobin.NewClientWrapper()),
	)

	paymentService := payment.NewPaymentService("payment", srv.Client())
	// Register handler
	if err = paymentApi.RegisterPaymentApiHandler(srv.Server(), &handler.PaymentApi{PaymentService: paymentService}); err != nil {
		common.Error(err)
	}
	// Run service
	if err = srv.Run(); err != nil {
		logger.Fatal(err)
	}
}

type clientWrapper struct {
	client.Client
}

func (c *clientWrapper) Call(ctx context.Context, req client.Request, rsp interface{}, opts ...client.CallOption) error {
	return hystrix.Do(req.Service()+"."+req.Endpoint(), func() error {
		//run 正常执行
		fmt.Println(req.Service() + "." + req.Endpoint())
		return c.Client.Call(ctx, req, rsp, opts...)
	}, func(err error) error {
		fmt.Println(err)
		return err
	})
}

func NewClientHystrixWrapper() client.Wrapper {
	return func(i client.Client) client.Client {
		return &clientWrapper{i}
	}
}
