package main

import (
	"fmt"
	consul2 "github.com/go-micro/plugins/v4/registry/consul"
	opentracing2 "github.com/go-micro/plugins/v4/wrapper/trace/opentracing"
	"github.com/opentracing/opentracing-go"
	"go-micro.dev/v4/registry"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"product/common"
	"product/domain/repository"
	service2 "product/domain/service"
	"product/handler"
	"product/internal"
	pb "product/proto/product"

	"go-micro.dev/v4"
	"go-micro.dev/v4/logger"
)

var (
	service = "product"
	version = "latest"
)

func main() {
	//配置中心
	consulConfig, err := common.GetConsulConfig("192.168.1.89", 8500, "/micro/config")
	if err != nil {
		logger.Error(err)
	}
	//注册中心
	consul := consul2.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{
			"192.168.1.89:8500",
		}
	})
	//链路追踪
	t, io, err := common.NewTracer("product", "192.168.1.89:6831")
	if err != nil {
		logger.Fatal(err)
	}
	defer io.Close()
	opentracing.SetGlobalTracer(t)
	//获取mysql配置,路径中不带前缀
	mysqlInfo, err := common.GetMysqlFromConsul(consulConfig, "mysql")
	if err != nil {
		logger.Fatal(err)
	}
	logger.Info("Mysql配置信息:", mysqlInfo)
	//创建数据库连接
	mysqlConfig := mysql.Config{
		DSN:                       fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", mysqlInfo.User, mysqlInfo.Pwd, mysqlInfo.Host, mysqlInfo.Port, mysqlInfo.Database),
		DefaultStringSize:         191,
		SkipInitializeWithVersion: false, // 根据版本自动配置
	}
	db, _ := gorm.Open(mysql.New(mysqlConfig), internal.Gorm.Config())
	sqlDb, err := db.DB()
	if err != nil {
		return
	}
	defer sqlDb.Close()
	//初始化
	//repository.NewProductRepository(db).InitTable()
	productDataService := service2.NewProductDataService(repository.NewProductRepository(db))
	// Create service
	srv := micro.NewService()
	srv.Init(
		micro.Name(service),
		micro.Version(version),
		//添加注册中心
		micro.Registry(consul),
		micro.WrapHandler(opentracing2.NewHandlerWrapper(opentracing.GlobalTracer())),
	)

	//
	// Register handler
	if err := pb.RegisterProductHandler(srv.Server(), &handler.Product{ProductDataService: productDataService}); err != nil {
		logger.Fatal(err)
	}
	// Run service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
