package main

import (
	"fmt"
	"gitee.com/qwert/common"
	"gitee.com/qwert/microservice_study/order/domain/repository"
	service2 "gitee.com/qwert/microservice_study/order/domain/service"
	"gitee.com/qwert/microservice_study/order/handler"
	pb "gitee.com/qwert/microservice_study/order/proto/order"
	consul2 "github.com/go-micro/plugins/v4/registry/consul"
	"github.com/go-micro/plugins/v4/wrapper/monitoring/prometheus"
	ratelimit "github.com/go-micro/plugins/v4/wrapper/ratelimiter/uber"
	opentracing2 "github.com/go-micro/plugins/v4/wrapper/trace/opentracing"
	"github.com/opentracing/opentracing-go"
	"go-micro.dev/v4"
	"go-micro.dev/v4/logger"
	"go-micro.dev/v4/registry"
	"go-micro.dev/v4/util/log"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	service = "order"
	version = "latest"
	QPS     = 1000
)

func main() {
	// 1.配置中心
	consulConfig, err := common.GetConsulConfig("192.168.1.89", 8500, "/micro/config")
	if err != nil {
		log.Error(err)
	}
	// 2.注册中心
	consul := consul2.NewRegistry(func(options *registry.Options) {
		options.Addrs = []string{
			"192.168.1.89:8500",
		}
	})
	// 3.jaeger 链路追踪
	t, io, err := common.NewTracer("order", "192.168.1.89:6831")
	if err != nil {
		log.Error(err)
	}
	defer io.Close()
	opentracing.SetGlobalTracer(t)
	//获取mysql配置,路径中不带前缀
	mysqlInfo, err := common.GetMysqlFromConsul(consulConfig, "mysql")
	if err != nil {
		logger.Fatal(err)
	}
	logger.Info("Mysql配置信息:", mysqlInfo)
	//创建数据库连接
	mysqlConfig := mysql.Config{
		DSN:                       fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", mysqlInfo.User, mysqlInfo.Pwd, mysqlInfo.Host, mysqlInfo.Port, mysqlInfo.Database),
		DefaultStringSize:         191,
		SkipInitializeWithVersion: false, // 根据版本自动配置
	}
	db, _ := gorm.Open(mysql.New(mysqlConfig), common.Gorm.Config())
	sqlDb, err := db.DB()
	if err != nil {
		return
	}
	defer sqlDb.Close()
	//第一次初始化
	/*	err = repository.NewOrderRepository(db).InitTable()
		if err != nil {
			log.Error(err)
		}*/
	//创建实例
	orderDataService := service2.NewOrderDataService(repository.NewOrderRepository(db))
	//暴露监控地址
	common.PrometheusBoot(9093)
	// Create service
	srv := micro.NewService()
	srv.Init(
		micro.Name(service),
		micro.Version(version),
		//暴露服务地址
		micro.Address(":9085"),
		//添加consul 注册中心
		micro.Registry(consul),
		//添加链路追踪
		micro.WrapHandler(opentracing2.NewHandlerWrapper(opentracing.GlobalTracer())),
		//添加限流
		micro.WrapHandler(ratelimit.NewHandlerWrapper(QPS)),
		//添加监控
		micro.WrapHandler(prometheus.NewHandlerWrapper()),
	)

	// Register handler
	if err := pb.RegisterOrderHandler(srv.Server(), &handler.Order{OrderDataService: orderDataService}); err != nil {
		logger.Fatal(err)
	}
	// Run service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
