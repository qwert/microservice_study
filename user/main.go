package main

import (
	"gorm.io/driver/mysql"
	_ "gorm.io/driver/mysql"
	"gorm.io/gorm"
	"user/domain/repository"
	service2 "user/domain/service"
	"user/handler"
	"user/internal"
	pb "user/proto"

	_ "github.com/go-micro/plugins/v4/registry/etcd"
	"go-micro.dev/v4"
	"go-micro.dev/v4/logger"
)

var (
	service = "user"
	version = "latest"
)

//配置中心 nacos  和 consul
func main() {
	// Create service
	srv := micro.NewService()
	srv.Init(
		micro.Name(service),
		micro.Version(version),
		/*	micro.Registry(etcd.NewRegistry(
			registry.Addrs("127.0.0.1:2379"),
		)),*/
	)
	//创建数据库连接
	mysqlConfig := mysql.Config{
		DSN:                       "root:123456@tcp(127.0.0.1:3306)/micro?charset=utf8mb4&parseTime=True&loc=Local",
		DefaultStringSize:         191,
		SkipInitializeWithVersion: false, // 根据版本自动配置
	}
	db, _ := gorm.Open(mysql.New(mysqlConfig), internal.Gorm.Config())
	sqlDb, err := db.DB()
	if err != nil {
		return
	}
	defer sqlDb.Close()
	//只执行一次，数据表初始化
	/*rp := repository.NewUserRepository(db)
	rp.InitTable()*/
	//创建服务实例
	userDataService := service2.NewUserDataService(repository.NewUserRepository(db))

	// Register handler
	if err := pb.RegisterUserHandler(srv.Server(), &handler.User{UserDataService: userDataService}); err != nil {
		logger.Fatal(err)
	}
	// Run service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
